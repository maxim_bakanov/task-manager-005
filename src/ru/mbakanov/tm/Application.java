package ru.mbakanov.tm;

import ru.mbakanov.tm.constant.TerminalConst;

public class Application {

    public static void main(final String[] args) {
        System.out.println("== Welcome to TASK MANAGER ==");
        parseArgs(args);
    }

    private static void parseArgs (final String[] args) {
        if (args.length!=0 & args!=null) {
            switch (args[0]) {
                case TerminalConst.HELP:
                    showHelp();
                    break;
                case TerminalConst.ABOUT:
                    showAbout();
                    break;
                case TerminalConst.VERSION:
                    showVersion();
                    break;
                default:
                    showUnexpected(args[0]);
                    break;
            }
        }
    }

    private static void showHelp()  {
        System.out.println("["+TerminalConst.HELP+"]");
        System.out.println(TerminalConst.ABOUT + " - Show developer info.");
        System.out.println(TerminalConst.VERSION + " - Show application version.");
        System.out.println(TerminalConst.HELP + " - Show terminal`s commands.");
    }

    private static void showVersion()  {
        System.out.println("["+ TerminalConst.VERSION +"]");
        System.out.println(TerminalConst.VERSION_NUM);
    }

    private static void showAbout()  {
        System.out.println("["+TerminalConst.ABOUT+"]");
        System.out.println(TerminalConst.DEV_NAME);
        System.out.println(TerminalConst.DEV_EMAIL);
    }

    private static void showUnexpected(String arg)  {
        System.out.println("['"+arg+"' - unexpected command]");
    }
}
